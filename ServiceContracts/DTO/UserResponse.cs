﻿using Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceContracts.DTO
{
    public class UserResponse
    {
        public Guid UserID { get; set; }

        public string? Password { get; set; }

        public string? Email { get; set; }

        public string? PhoneNumber { get; set; }

        public bool IsAdmin { get; set; }
    }

    public static class Extension
    {
        public static UserResponse ToUserResponse(this User user)
        {
            return new UserResponse()
            {
                UserID = user.UserID,
                Password = user.Password,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
                IsAdmin = user.IsAdmin,

            };
        }
    }
}
