﻿using Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceContracts.DTO
{
    public class UserLoginReq
    {
        [Key]
        public Guid UserID { get;}

        [Required]
        [DataType(DataType.Password)]
        public string? Password { get; set; }

        [Required]
        public string? Email { get; set; }

        private readonly AppDbContext _db;

        public User ToLoginReq()
        {
            return new User() 
            { 
                Email = Email, 
                Password = Password 
            };
        }
    }
}
