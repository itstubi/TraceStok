﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;

namespace ServiceContracts.DTO
{
    public class ProductUpdateReq
    {
        [Key]
        public Guid ProductID { get; set; }

        public string? ProductName { get; set; }

        public float ProductPrice { get; set; }

        public int? ProductQuantity { get; set; }

        public Product ToProduct()
        {
            return new Product()
            {
                ProductID = ProductID,
                ProductName = ProductName,
                ProductPrice = ProductPrice,
                ProductQuantity = ProductQuantity,
            };
        }
    }
}
