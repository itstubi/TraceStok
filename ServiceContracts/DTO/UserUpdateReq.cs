﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;

namespace ServiceContracts.DTO
{
    public class UserUpdateReq
    {
        [Key]
        public Guid UserID { get; set; }

        public string? Password { get; set; }

        [Required]
        public string? Email { get; set; }

        public string? PhoneNumber { get; set; }

        public bool IsAdmin { get; set; }

        public User ToUser()
        {
            return new User()
            {
                Password = Password,
                Email = Email,
                PhoneNumber = PhoneNumber,
                IsAdmin = IsAdmin,
            };
        }
    }
}
