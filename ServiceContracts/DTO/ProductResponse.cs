﻿using Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceContracts.DTO
{
    public class ProductResponse
    {
        [Key]
        public Guid ProductID { get; set; }

        public string? ProductName { get; set; }

        public float ProductPrice { get; set; }

        public int? ProductQuantity { get; set; }

        public ProductUpdateReq ToProductUpdateReq()
        {
            return new ProductUpdateReq()
            {
                ProductID = ProductID,
                ProductName = ProductName,
                ProductPrice = ProductPrice,
                ProductQuantity = ProductQuantity,
            };
        }
    }

    public static class ProductExtension
    {
        public static ProductResponse ToProductResponse(this Product product)
        {
            return new ProductResponse()
            {
                ProductID = product.ProductID,
                ProductName  = product.ProductName,
                ProductPrice = product.ProductPrice,
                ProductQuantity = product.ProductQuantity,
            };
        }
    }
}
