﻿using ServiceContracts.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceContracts
{
    public interface IProductsService
    {
        public ProductResponse AddProduct(ProductAddReq? productAddReq);
        public bool DelProduct(Guid? productID);
        public List<ProductResponse> GetAllProducts();
        public ProductResponse GetProductByID(Guid? productID);
        public ProductResponse UpdateProduct(ProductUpdateReq? productUpdateReq);
    }
}
