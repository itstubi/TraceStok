﻿using ServiceContracts.DTO;

namespace ServiceContracts
{
    public interface IUsersService
    {
        public UserResponse AddUser(UserAddReq? userAddReq);
        public List<UserResponse> GetAllUsers();
        public UserResponse? GetUserByUserID(Guid? userID);
        public bool DeleteUser(Guid? userID);
        public UserResponse UpdateUser(UserUpdateReq? userUpdateReq);
        public bool UserResponse(UserLoginReq? userLoginReq);
    }
}