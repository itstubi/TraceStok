﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class Product
    {
        [Key]
        public Guid ProductID {  get; set; }

        public string? ProductName { get; set; }

        public float ProductPrice { get; set; }

        public int? ProductQuantity { get; set; }
    }
}
