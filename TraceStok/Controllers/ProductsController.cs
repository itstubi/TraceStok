﻿using Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using ServiceContracts;
using ServiceContracts.DTO;

namespace TraceStok.Controllers
{
    [Route("[controller]")]
    public class ProductsController : Controller
    {
        private readonly IProductsService _productsService;

        public ProductsController(IProductsService productsService)
        {
            _productsService = productsService;
        }

        [Route("[action]/{SearchString}")]
        [Route("")]
        [Route("/")]
        public IActionResult Products(string? SearchString)
        {
            if (String.IsNullOrEmpty(SearchString))
            {
                List<ProductResponse> allProduccts = _productsService.GetAllProducts();

                return View(allProduccts);
            }

            List<ProductResponse> products = _productsService.GetAllProducts().Where(p => p.ProductName.Contains(SearchString)).ToList();

            return View(products);

        }

        [Route("[action]")]
        [HttpGet]
        public IActionResult AddProduct()
        {
            return View();
        }

        [Route("[action]")]
        [HttpPost]
        public IActionResult AddProduct(ProductAddReq productAddReq)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            ProductResponse productResponse = _productsService.AddProduct(productAddReq);

            return RedirectToAction("Products");
        }

        [Route("[action]")]
        [HttpGet]
        public IActionResult Delete(Guid? productID)
        {
            ProductResponse? productResponse = _productsService.GetProductByID(productID);
            if (productResponse == null)
            {
                return RedirectToAction("Products");
            }

            return View(productResponse);
        }

        [Route("[action]")]
        [HttpPost]
        public IActionResult Delete(Product product)
        {
            ProductResponse? productResponse = _productsService.GetProductByID(product.ProductID);
            if (productResponse == null)
            {
                return RedirectToAction("Products");
            }

            _productsService.DelProduct(product.ProductID);

            return RedirectToAction("Products");
        }

        [Route("[action]/{productID}")]
        [HttpGet]
        public IActionResult Update(Guid? productID) 
        {
            ProductResponse? productResponse = _productsService.GetProductByID(productID);
            if (productResponse == null)
            {
                return RedirectToAction("Products");
            }

            ProductUpdateReq productUpdateReq = productResponse.ToProductUpdateReq();

            return View(productUpdateReq);
        }

        [Route("[action]/{productID}")]
        [HttpPost]
        public IActionResult Update(ProductUpdateReq productUpdateReq)
        {
            ProductResponse? productResponse = _productsService.GetProductByID(productUpdateReq.ProductID);
            if (productResponse == null)
            {
                return RedirectToAction("Products");
            }

            if(!ModelState.IsValid) 
            {
                return View(productResponse.ToProductUpdateReq());
            }

            _productsService.UpdateProduct(productUpdateReq);

            return RedirectToAction("Products");
        }
    }
}
