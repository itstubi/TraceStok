﻿using Microsoft.AspNetCore.Mvc;
using ServiceContracts;

namespace TraceStok.Controllers
{
    [Route("[controller]")]
    public class UsersController : Controller
    {
        private readonly IUsersService _usersService;

        public UsersController(IUsersService usersService)
        {
            _usersService = usersService;
        }

        [Route("/login")]
        public IActionResult LoginPage()
        {
            return View();
        }

        [Route("/register")]
        public IActionResult Register()
        {
            return View();
        }

        [Route("/usermanagment&{userID}")]
        public IActionResult UserManagment()
        {
            return View();
        }
    }
}
