﻿using Entities;
using Services.Helpers;
using ServiceContracts.DTO;
using System;
using ServiceContracts;

namespace Services
{
    public class UsersService : IUsersService
    {
        private readonly AppDbContext _db;

        public UsersService(AppDbContext db)
        {
            _db = db;
        }

        public UserResponse ConvertUserToUserResponse(User user)
        {
            UserResponse userResponse = user.ToUserResponse();

            return userResponse;
        }

        public UserResponse AddUser(UserAddReq? userAddReq)
        {
            if (userAddReq == null) { throw new ArgumentNullException(nameof(userAddReq));}

            ValidationHelper.ModelValitation(userAddReq);

            User user = userAddReq.ToUser();

            user.UserID = Guid.NewGuid();

            _db.Users.Add(user);
            _db.SaveChanges();
            
            return ConvertUserToUserResponse(user);
        }

        public bool DeleteUser(Guid? userID)
        {
            if (userID == null) { throw new ArgumentNullException(nameof(userID)); }

            User? user = _db.Users.FirstOrDefault(u => u.UserID == userID);
            if (user == null) { return false; }

            _db.Users.Remove(user);
            _db.SaveChanges();
            return true;
        }

        public List<UserResponse> GetAllUsers()
        {
            return _db.Users.ToList().Select(user => ConvertUserToUserResponse(user)).ToList();
        }

        public UserResponse? GetUserByUserID(Guid? userID)
        {
            if (userID == null) { return null; }

            User? user = _db.Users.FirstOrDefault(user => user.UserID == userID);
            if (user == null) { return null; }

            return ConvertUserToUserResponse(user);
        }

        public UserResponse UpdateUser(UserUpdateReq? userUpdateReq)
        {
            if(userUpdateReq  == null) { throw new ArgumentNullException( nameof(userUpdateReq)); }

            ValidationHelper.ModelValitation(userUpdateReq);

            User? user = _db.Users.FirstOrDefault(user => user.UserID == userUpdateReq.UserID);
            if (user == null) { throw new ArgumentException("Given user id doesn't exist."); }

            _db.SaveChanges();

            return ConvertUserToUserResponse(user);
        }

        bool IUsersService.UserResponse(UserLoginReq? userLoginReq)
        {
            User? user = _db.Users.FirstOrDefault(u => u.Email == userLoginReq.Email && u.Password == userLoginReq.Password);

            if (user == null)
            {
                throw new ArgumentException("User Doesn't exist.");
            }
            return true;
        }
    }
}