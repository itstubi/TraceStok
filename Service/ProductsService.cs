﻿using Entities;
using Services.Helpers;
using ServiceContracts;
using ServiceContracts.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class ProductsService : IProductsService
    {
        private readonly AppDbContext _db;

        public ProductsService(AppDbContext db)
        {
            _db = db;
        }

        public ProductResponse ConvertProduct2ProductResponse(Product product)
        {
            ProductResponse productResponse = product.ToProductResponse();

            return productResponse;
        }

        public ProductResponse AddProduct(ProductAddReq? productAddReq)
        {
            if (productAddReq == null) { throw new ArgumentNullException(nameof(productAddReq)); };

            ValidationHelper.ModelValitation(productAddReq);

            Product product = productAddReq.ToProduct();
            product.ProductID = Guid.NewGuid();

            _db.Products.Add(product);
            _db.SaveChanges();

            return ConvertProduct2ProductResponse(product);
        }

        public bool DelProduct(Guid? productID)
        {
            if (productID == null) { throw new ArgumentNullException(nameof(productID)); };

            Product? product = _db.Products.FirstOrDefault(p => p.ProductID == productID);
            if (product == null) { return false; }

            _db.Products.Remove(product);
            _db.SaveChanges();
            return true;
        }

        public List<ProductResponse> GetAllProducts()
        {
            return _db.Products.ToList().Select(product => ConvertProduct2ProductResponse(product)).ToList();
        }

        public ProductResponse GetProductByID(Guid? productID)
        {
            if (productID == null) { throw new ArgumentNullException(null);}

            Product? product = _db.Products.FirstOrDefault(p => p.ProductID == productID);
            if(product == null) { throw new ArgumentException("Given product id doesn't exist."); }

            return ConvertProduct2ProductResponse(product);
        }

        public ProductResponse UpdateProduct(ProductUpdateReq? productUpdateReq)
        {
            if (productUpdateReq == null) { throw new ArgumentNullException(nameof(productUpdateReq)); }

            ValidationHelper.ModelValitation(productUpdateReq);

            Product? product = _db.Products.FirstOrDefault(p => p.ProductID == productUpdateReq.ProductID);
            if(product == null) { throw new ArgumentException("Given product id doesn't exist."); }

            product.ProductID = productUpdateReq.ProductID;
            product.ProductName = productUpdateReq.ProductName;
            product.ProductPrice = productUpdateReq.ProductPrice;
            product.ProductQuantity = productUpdateReq.ProductQuantity;

            _db.SaveChanges();
            return ConvertProduct2ProductResponse(product);
        }
    }
}
